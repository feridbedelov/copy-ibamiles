$(document).ready(function() {
    $('select').niceSelect();

    $('.home-slider').slick({
        autoplay: true,
        autoplaySpeed: 7000,
        arrows: false,
        fade: true,
        dots: true,
        pauseOnHover: false
    });

    $(document).on('click', '.partners-modal .close', function(){
        $('body').find('.partners-modal').fadeOut();
    });

    $(document).on('click', '.partners-modal .close-part', function(){
        $('body').find('.partners-modal').fadeOut();
    });

    $(document).on('click', '.partners-list .item', function(){
        $('body').find('.partners-modal').fadeIn(500);
    });

    function calculator_slider() {
        $('.calculator-header').slick({
            asNavFor: '.calculator-self',
            mobileFirst: true,
            responsive: [
                {
                        breakpoint: 651,
                        settings: 'unslick'
                }
            ]
        });
    
        $('.calculator-self').slick({
            adaptiveHeight: true,
            dots: true,
            asNavFor: '.calculator-header',
            draggable: false,
            mobileFirst: true,
            responsive: [
                {
                        breakpoint: 651,
                        settings: 'unslick'
                }
            ]
        });
    }


    $(window).on('load resize orientationchange', function() {
        $('.carousel').each(function(){
            var $carousel = $(this);
            if ($(window).width() > 651) {
                if ($carousel.hasClass('slick-initialized')) {
                    $carousel.slick('unslick');
                }
            }
            else{
                if (!$carousel.hasClass('slick-initialized')) {
                    $('.calculator-header').slick({
                        asNavFor: '.calculator-self',
                        mobileFirst: true
                    });
                
                    $('.calculator-self').slick({
                        adaptiveHeight: true,
                        dots: true,
                        arrows: false,
                        asNavFor: '.calculator-header',
                        draggable: false, 
                        mobileFirst: true
                    });
                }
            }
        });
    });

    $(document).on('click', '.buttons .details', function(){
        $(this).closest('.item').find('.more-part').slideToggle();
    })
});
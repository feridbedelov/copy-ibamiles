import React, { useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import "./App.css";
import { Layout, Landing } from "./components";

import {
  setTranslations,
  setDefaultLanguage,
  setLanguage,
} from "react-switch-lang";
import en from "./lang/en.json";
import az from "./lang/az.json";
import ru from "./lang/ru.json";

setTranslations({ az, en, ru });

function App() {
  // Set localstorage for lang
  useEffect(() => {
    !localStorage.getItem("lang") && localStorage.setItem("lang", "az");
  }, []);


  // Setting deafult lang
  useEffect(() => {
    localStorage.getItem("lang")
      ? setDefaultLanguage(localStorage.getItem("lang"))
      : setLanguage(az);
  }, []);


  return (
    <div>
      <Layout>
        <Switch>
          <Route
            component = {Landing}
            exact
            path='/'
          />
        </Switch>
      </Layout>
    </div>
  );
}

export default App;

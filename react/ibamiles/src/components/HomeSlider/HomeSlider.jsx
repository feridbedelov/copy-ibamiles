import React from "react";
import sliderItem1 from "../../assets/img/home-slider-item-1.png";
import sliderItem2 from "../../assets/img/home-slider-item-2.png";
import sliderItemShadow from "../../assets/img/home-slider-item-shadow.png";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export const HomeSlider = () => {
  let settings = {
    autoplay: true,
    autoplaySpeed: 7000,
    arrows: false,
    fade: true,
    dots: true,
    pauseOnHover: false
  };

  return (
    <section className='home-slider'>
      <Slider {...settings}>
        <div className='item'>
          <div className='container'>
            <div className='texts'>
              <p className='bold'>IBAMiles</p>
              <p className='normal'>Black Edition</p>
            </div>

            <div className='card-img'>
              <img src={sliderItem1} alt='' />
              <img src={sliderItemShadow} alt='' className='shadow' />
            </div>
          </div>
        </div>

        <div className='item'>
          <div className='container'>
            <div className='texts'>
              <p className='bold'>IBAMiles</p>
              <p className='normal'>World</p>
            </div>

            <div className='card-img'>
              <img src={sliderItem2} alt='' />
              <img src={sliderItemShadow} alt='' className='shadow' />
            </div>
          </div>
        </div>
      </Slider>
    </section>
  );
};

import React, { Fragment } from 'react'
import {Header} from "../Header/Header"
import {Footer} from "../Footer/Footer"

export const Layout = (props) => {
  return (
    <Fragment>
      <Header />
      <Fragment>
        {props.children}
      </Fragment>
      <Footer />
    </Fragment>
  )
}

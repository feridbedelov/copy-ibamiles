import React from "react";
import "./Footer.css"
import {Link} from "react-router-dom";
import { translate } from 'react-switch-lang';

export const Footer = translate(({t}) => {
  return (
    <footer>
      <div className='footer-top'>
        <div className='container'>
          {/* <div className='left'>
            <ul>
              <li>
                <Link to="/">{t('footer.homePage')}</Link>
              </li>
              <li>
                <Link to="/personalCabinet" >{t('footer.personalCabinet')}</Link>
              </li>
            </ul>
          </div> */}

          <div className='center'>
            <p>{t('footer.followUs')}</p>

            <div className='links'>
              <a href='https://www.facebook.com/azerbaycanbeynelxalqbanki/'>
                <img src={require("../../../assets/img/fb.svg")} alt='' />
              </a>

              <a href='https://www.instagram.com/azerbaycan_beynelxalq_banki/'>
                <img src={require("../../../assets/img/instagram.svg")} alt='' />
              </a>
            </div>
          </div>

          <div className='right'>
            <p>{t('footer.mobileBanking')}</p>

            <div className='links'>
              <Link to='#'>
                <img src={require("../../../assets/img/play_store.svg")} alt='' />
              </Link>

              <Link to='#'>
                <img src={require('../../../assets/img/app_store.svg')} alt='' />
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div className='footer-bottom'>
        <div className='container'>
          <p className='left'>{t('footer.rights')}</p>
          <p className='right'>IBA © 2020</p>
        </div>
      </div>
    </footer>
  );
});

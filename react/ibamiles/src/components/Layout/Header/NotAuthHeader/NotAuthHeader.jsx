import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import $ from "jquery";
import { customOption } from "../useHeader";
import useWindowDimensions from "../../../../Utils/useWindowDimensions";
import { setLanguage, translate, getLanguage } from 'react-switch-lang';

export const NotAuthHeader = translate((props) => {
  const [runSelect, setRunSelect] = useState(true);
  const [firstRun, setFirstRun] = useState(true);
  const { t } = props;

  const initialLang = () => {
    if (localStorage.getItem('lang'))
    return localStorage.getItem('lang').toUpperCase() || "AZ"
  }
  $(".lang-selected").html(initialLang());
  const [lang, setLang] = useState(initialLang());

  useEffect(() => {
    $(document).ready(() => {
      runSelect && customOption("lang-select");
      setRunSelect(false);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  useEffect(() => {
    $(document).ready(() => {
      $(".lang-items").on("click", (e) => {
        setLang(e.target.innerHTML);
        window.location.reload();
      });
    });
  }, [])

  useEffect(() => {
    // if (!firstRun)
    // $(".lang-selected").html(lang);

    if (!firstRun)
    switch (lang) {
      case 'AZ':
        console.log("az");
        setLanguage('az');
        localStorage.setItem('lang', getLanguage());
        break;
      case 'EN':
        console.log("en");
        setLanguage('en');
        localStorage.setItem('lang', getLanguage());
        break;
      case 'RU':
        console.log("ru");
        setLanguage('ru');
        localStorage.setItem('lang', getLanguage());
        break;

      default:
        break;
    }
    setFirstRun(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lang]);

  const { width } = useWindowDimensions();


  return (
    <Fragment>
      <header>
        <ul>
          <li className='mob-cabinet'>
            <Link to='/'>
              <img
                src={require("../../../../assets/img/personal-cabinet.svg")}
                alt='person'
              />
            </Link>
          </li>
          <li className='left'>
            <Link to='/'>
              <img
                src={require("../../../../assets/img/ibar-logo.svg")}
                alt='logo'
              />
            </Link>
          </li>

          <li className='right'>
            <Link onClick={props.openModal} to='#'>
              <img
                src={require("../../../../assets/img/person-icon.svg")}
                alt=''
              />
              <span>{t('home.login.personalCabinet')}</span>
            </Link>

            <div
              className='custom-select lang-select'
              style={{ width: "55px" }}
            >
              <select>
                <option value='0' lang-type='az'>
                  AZ
                </option>
                <option value='0' lang-type='az'>
                  AZ
                </option>
                <option value='1' lang-type='ru'>
                  RU
                </option>
                <option value='2' lang-type='en'>
                  EN
                </option>
              </select>
            </div>
          </li>
        </ul>
      </header>

      
    </Fragment>
  );
});

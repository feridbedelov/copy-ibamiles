import React from "react";
import "./Header.css";

import { NotAuthHeader } from "./NotAuthHeader/NotAuthHeader";

export const Header = (props) => {
  return (
    <NotAuthHeader
      isAuth={false}
    />
  );
};

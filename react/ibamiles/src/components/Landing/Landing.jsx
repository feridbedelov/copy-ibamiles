import React, { Fragment, useEffect, useState } from "react";
import { DebitCardInfoContainer } from "../DebitCardInfo";
import { HomeSlider } from "../HomeSlider";
import { Spinner } from "../Spinner";

export const Landing = ({}) => {
  const [initLoading, setInitLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setInitLoading(false);
    }, 1200);
  }, []);

  return initLoading ? (
    <div className='page_loading'>
      <Spinner />
    </div>
  ) : (
    <Fragment>
      <HomeSlider />
      <DebitCardInfoContainer />
    </Fragment>
  );
};

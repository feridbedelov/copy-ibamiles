import React, { Fragment } from "react";
import PropTypes from 'prop-types';
import { DebitCardItem } from "../DebitCardItem/DebitCardItem";
import { translate } from 'react-switch-lang';

export const DebitCardInfoDesktop = translate(({t}) => {

  const cardSimple = {
    type:"simple",
    productName:"MC World Debit Mil Card AZN"
  }

  const cardBlack = {
    type:"black",
    productName:"MC Black Edition Miles PayPass Card AZN MChip"
  }

  return (
    <Fragment>
      <section className='card-info-part'>
        <div className='container'>
          <div className='title'>
          <p>{t(`home.debitCardInfoTitle`)}</p>
          </div>

          <ul className='cards'>
          <DebitCardItem card = {cardSimple} />
            <DebitCardItem card = {cardBlack} />
          </ul>
        </div>
      </section>

      
    </Fragment>
  );
});

DebitCardInfoDesktop.propTypes = {
  t: PropTypes.func.isRequired,
};
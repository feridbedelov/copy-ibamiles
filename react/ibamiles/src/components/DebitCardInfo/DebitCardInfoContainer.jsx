import React from "react";
import useWindowDimensions from "../../Utils/useWindowDimensions";
import { DebitCardInfoMobile } from "./MobileView/DebitCardInfoMobile";
import { DebitCardInfoDesktop } from "./DesktopView/DebitCardInfoDesktop";




export const DebitCardInfoContainer = props => {

  // This function gets the window width every time it changes
  const { width } = useWindowDimensions();

  let view = (
    <DebitCardInfoDesktop
    />
  );

  if (width < 768)
    view = (
      <DebitCardInfoMobile
      />
    );

  return  view;
};

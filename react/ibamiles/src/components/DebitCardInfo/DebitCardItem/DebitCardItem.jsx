import React, { useState } from "react";
import simpleCard from "../../../assets/img/IbaMiles-card-1.png";
import blackCard from "../../../assets/img/IbaMiles-card-2.png";
import "./DebitCardItem.scss";
import { useTransition, animated } from "react-spring";
import {translate} from 'react-switch-lang';

export const DebitCardItem = translate(({ t, card }) => {
  const [extended, setExtended] = useState(false);

  const transitions = useTransition(extended, null, {
    from: { opacity: 0, height: 0, overflow: "hidden" },
    enter: { opacity: 1, height: "auto", overflow: "auto" },
    leave: { opacity: 0, height: 0, overflow: "hidden" },
  });

  let cardType = simpleCard;
  if( card.type === "black" ){
    cardType = blackCard
  }


  return (
    <li className='item'>
      <img src={cardType} alt='card' />

      <h3>{card.productName}</h3>

      <p>{t('home.cardInfo.cardCurrency')} — AZN</p>

      <p>{t('home.cardInfo.cardDuration')} — 3 {t('home.cardInfo.years')}</p>

      <p>{t('home.cardInfo.welcomeMiles')} — 5000 {t('home.cardInfo.miles')}</p>

      {transitions.map(({ item, key, props }) => {
        return (
          item && (
            <animated.div key = {key} style={props}>
              <p>{t('home.cardInfo.welcomeMilesAmount')} — 1000 {t('home.cardInfo.AZN')}</p>
              <p>{t('home.cardInfo.SMSnotify')} — {t('home.cardInfo.free')}</p>
              <p>{t('home.cardInfo.cashWithdrawal')} — {t('home.cardInfo.free')}</p>
              <p>{t('home.cardInfo.cardPrice')} — 80 {t('home.cardInfo.AZN')}</p>
            </animated.div>
          )
        );
      })}

      <div className='buttons'>
        <button
          onClick={() => setExtended((prev) => !prev)}
          className='details'
        >
          {t('home.cardInfo.details')}
        </button>

        <span  className='order'>
          {t('home.cardInfo.order')}
        </span>
      </div>
    </li>
  );
});

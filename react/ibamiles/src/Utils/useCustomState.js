import { useState } from "react";

const useCustomState = (initialValues) => {
  const [orderCardValues, setOrderCardValues] = useState(initialValues);
  const onChangeHandlerForOrderCard = (e) => {
    if (e.target.name === "finCode") {

      setOrderCardValues({
        ...orderCardValues,
        finCode : e.target.value.toUpperCase() 
      })

    }else if(e.target.name === "passportId"){
      setOrderCardValues({
        ...orderCardValues,
        passportId : e.target.value.toUpperCase()
      })
    }
    else if(e.target.name === "name" || e.target.name=== "surname" || e.target.name === "patronymic" ){
      setOrderCardValues({
        ...orderCardValues,
        [e.target.name] : e.target.value.toUpperCase()
      })
    } 
    else if(e.target.name === "image1" || e.target.name === "image2" ){
      setOrderCardValues({
        ...orderCardValues,
        [e.target.name] : e.target.files[0]  ? e.target.files[0] : null
      })
    }
    else {
      setOrderCardValues({
        ...orderCardValues,
        [e.target.name]: e.target.value,
      });
    }
  };

  return [orderCardValues, onChangeHandlerForOrderCard];
};

export default useCustomState;
